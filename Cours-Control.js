/*
    Exercices basiques :

    Temps prévue : 1heure

 */

/*
    Exercice 1 :
    Déclarer une variable et une constante contenant les valeurs de votre choix




 */

/*
    Exercice 2 :
    Déclarer une fonction nommé "getRandomNumber" qui
    ne prends aucun paramètre et retourne un nombre aléatoire entre 0 et 1
    Invoquez la fonction 2 fois avec des arguments différents
 */






/*
    Exercice 4 :
    Déclarer une variable "arr1" qui contient un tableau vide.
    Ajoutez trois valeurs, le nombre 22, puis le nombre 30 et pour finir le nombre 16;
    1. Affichez la deuxième valeur dans la console
    2. Supprimez la deuxième valeur du tableau arr1
    3. Supprimez la dernière valeur du tableau arr1
    4. Supprimez la première valeur
    5. Affichez le tableau dans la console
 */

/*
    Exercice 5 :
    Déterminez et affichez dans la console si la phrase contenu dans la variable "sentence" contient le mot "radieuse"
    Ajouter une condition qui supprime le mot radieuse si la phrase contient le mot radieuse sinon affichez dans la console "La phrase ne contient pas le mot "radieuse"".
 */

let sentence = "Ma journée est radieuse";


/*
    Ecercice 6 :
    Déclarer une variable nommée "prof" qui contient un objet vide.
    Ajoutez la propriété "name" à l'objet "prof" avec comme valeur "Jean"
    Ajoutez la propriété "students" à l'objet "prof" qui est un tableau de noms, choisissez 3 noms de votre choix à inséré dans la propriété "students"
    Affichez dans la console le nom du prof ainsi que le nom des éléments joints par une virgule et un espace ', '
    Remarque : Utilisez une méthode pour joindre les noms.
 */

/*
    Exercice 7 :
    Ajoutez à l'objet "prof" une méthode nommée "showName" qui affiche la propriété "name" de l'objet courrant
 */

/*
    Exercice 8 :
    Déclarer une classe Person avec un constructor qui prend en paramètre "name"
    Déclarer la variable "person1" qui contient une nouvelle instence de la classe Person
    Déclarer la variable "person2" qui contient une nouvelle instence de la classe Person, donnez lui un nom différent du premier
    Ajouter à la classe Personne une méthode nommée "showName" qui affiche dans la console le nom de la personne courrante
    Et exécuter la methode "showName" via la variable "person1" et "person2"
 */

/*
    Exercice 9 :
    Déclarer une boucle qui commence à 0 et qui incrément tant que i est inférieur à la longueur du tableau arr9
    Affichez dans la console chaque élément du tableau arr9
 */

const arr9 = [0, 2, 334, 53, 529, 95];


/*
    Exercice 10 :
    Créer un nouveau tableau à partir du tableau arr9 et multipliez chaque élément du tableau par 10;
    Résulat souhaité : [0, 20, 3340, 530, 5290, 950]
 */




/*
    Exercice 3 :
    Déclarer une fonction nommé "multiplyBy" qui prend en paramètre :
    - "nb" : le nombre à multiplier
    - "nb2" : le nombre multiplicateur
    retourne le produit des deux nombres passé en paramètre
    Invoquez la fonction avec des arguments
 */


/*
    Exercice 4 :
    Déclarer une variable "arr1" qui contient un tableau vide.
    Ajoutez trois valeurs, le nombre 22, puis le nombre 30 et pour finir le nombre 16;
    1. Affichez la deuxième valeur dans la console
    2. Supprimez la deuxième valeur du tableau arr1
    3. Supprimez la dernière valeur du tableau arr1
    4. Supprimez la première valeur
    5. Affichez le tableau dans la console
 */

/*
    Exercice 5 :
    Déterminez et affichez dans la console si la phrase contenu dans la variable "sentence" contient le mot "radieuse"
    Ajouter une condition qui supprime le mot radieuse si la phrase contient le mot radieuse sinon affichez dans la console "La phrase ne contient pas le mot "radieuse"".
 */

let sentence = "Ma journée est radieuse";


/*
    Ecercice 6 :
    Déclarer une variable nommée "prof" qui contient un objet vide.
    Ajoutez la propriété "name" à l'objet "prof" avec comme valeur "Jean"
    Ajoutez la propriété "students" à l'objet "prof" qui est un tableau de noms, choisissez 3 noms de votre choix à inséré dans la propriété "students"
    Affichez dans la console le nom du prof ainsi que le nom des éléments joints par une virgule et un espace ', '
    Remarque : Utilisez une méthode pour joindre les noms.
 */

/*
    Exercice 7 :
    Ajoutez à l'objet "prof" une méthode nommée "showName" qui affiche la propriété "name" de l'objet courrant
 */

/*
    Exercice 8 :
    Déclarer une classe Person avec un constructor qui prend en paramètre "name"
    Déclarer la variable "person1" qui contient une nouvelle instence de la classe Person
    Déclarer la variable "person2" qui contient une nouvelle instence de la classe Person, donnez lui un nom différent du premier
    Ajouter à la classe Personne une méthode nommée "showName" qui affiche dans la console le nom de la personne courrante
    Et exécuter la methode "showName" via la variable "person1" et "person2"
 */

/*
    Exercice 9 :
    Déclarer une boucle qui commence à 0 et qui incrément tant que i est inférieur à la longueur du tableau arr9
    Affichez dans la console chaque élément du tableau arr9
 */

const arr9 = [0, 2, 334, 53, 529, 95];


/*
    Exercice 10 :
    Créer un nouveau tableau à partir du tableau arr9 et multipliez chaque élément du tableau par 10;
    Résulat souhaité : [0, 20, 3340, 530, 5290, 950]
 */
